﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Text;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using System.Windows;

namespace Core.ViewModels
{
    class MainWindowViewModel : BindableBase
    {
        private string sourceCode;

        public string SourceCode { get => sourceCode; set => SetProperty(ref sourceCode, value); }

        public DelegateCommand RunCommand { get; }

        public MainWindowViewModel()
        {
            RunCommand = new DelegateCommand(Run);
        }

        private void Run()
        {
            var syntaxTree = SyntaxFactory.ParseSyntaxTree(SourceText.From(sourceCode));

            var systemAssemblyDir = Path.GetDirectoryName(typeof(object).GetTypeInfo().Assembly.Location);

            var assemblyPath = Path.ChangeExtension(Path.GetTempFileName(), "exe");

            var compilation = CSharpCompilation.Create(Path.GetFileName(assemblyPath))
                .WithOptions(new CSharpCompilationOptions(OutputKind.ConsoleApplication))
                .AddReferences(
                    MetadataReference.CreateFromFile(typeof(object).GetTypeInfo().Assembly.Location),
                    MetadataReference.CreateFromFile(typeof(Console).GetTypeInfo().Assembly.Location),
                    MetadataReference.CreateFromFile(Path.Combine(systemAssemblyDir, "System.Runtime.dll"))
                )
                .AddSyntaxTrees(syntaxTree);

            var result = compilation.Emit(assemblyPath);

            if (result.Success)
            {
                //Process.Start(assemblyPath);
                File.WriteAllText(
                    Path.ChangeExtension(assemblyPath, "runtimeconfig.json"),
                    GenerateRuntimeConfig()
                );
                var process = Process.Start("dotnet", assemblyPath);
            }
            else
            {
                MessageBox.Show(string.Join(Environment.NewLine, result.Diagnostics.Select(diagnostic => diagnostic.ToString())));
            }
        }

        private string GenerateRuntimeConfig()
        {
            var version = RuntimeInformation.FrameworkDescription;

            using (var stream = new MemoryStream())
            {
                using (var writer = new Utf8JsonWriter(
                    stream,
                    new JsonWriterOptions() { Indented = true }
                ))
                {
                    writer.WriteStartObject();
                    writer.WriteStartObject("runtimeOptions");
                    writer.WriteStartObject("framework");
                    writer.WriteString("name", "Microsoft.NETCore.App");
                    writer.WriteString(
                        "version",
                        RuntimeInformation.FrameworkDescription.Replace(".NET Core ", "")
                    );
                    writer.WriteEndObject();
                    writer.WriteEndObject();
                    writer.WriteEndObject();
                }

                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }
    }
}
