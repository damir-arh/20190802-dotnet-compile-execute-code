﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Text;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace Framework.ViewModels
{
    class MainWindowViewModel : BindableBase
    {
        private string sourceCode;

        public string SourceCode { get => sourceCode; set => SetProperty(ref sourceCode, value); }

        public DelegateCommand RunCommand { get; }

        public MainWindowViewModel()
        {
            RunCommand = new DelegateCommand(Run);
        }

        private void Run()
        {
            var syntaxTree = SyntaxFactory.ParseSyntaxTree(SourceText.From(sourceCode));

            var systemAssemblyDir = Path.GetDirectoryName(typeof(object).GetTypeInfo().Assembly.Location);

            var assemblyPath = Path.ChangeExtension(Path.GetTempFileName(), "exe");

            var compilation = CSharpCompilation.Create(Path.GetFileName(assemblyPath))
                .WithOptions(new CSharpCompilationOptions(OutputKind.ConsoleApplication))
                .AddReferences(
                    MetadataReference.CreateFromFile(typeof(object).GetTypeInfo().Assembly.Location),
                    MetadataReference.CreateFromFile(typeof(Console).GetTypeInfo().Assembly.Location),
                    MetadataReference.CreateFromFile(Path.Combine(systemAssemblyDir, "System.Runtime.dll"))
                )
                .AddSyntaxTrees(syntaxTree);

            var result = compilation.Emit(assemblyPath);

            if (result.Success)
            {
                Process.Start(assemblyPath);
            }
            else
            {
                MessageBox.Show(string.Join(Environment.NewLine, result.Diagnostics.Select(diagnostic => diagnostic.ToString())));
            }
        }
    }
}
